package com.binarystudio.academy.springsecurity.domain.hotel;

import com.binarystudio.academy.springsecurity.domain.hotel.model.Hotel;
import com.binarystudio.academy.springsecurity.domain.user.UserRepository;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.domain.user.model.UserRole;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

@Service
public class HotelService {
	private final HotelRepository hotelRepository;
	private final UserRepository userRepository;

	public HotelService(HotelRepository hotelRepository, UserRepository userRepository) {
		this.hotelRepository = hotelRepository;
		this.userRepository = userRepository;
	}

	public void delete(UUID hotelId) {
		// 4. todo: only the owner of the hotel or admin may delete the hotel
		var hotel = hotelRepository.getById(hotelId).get();
		var owner = getAuthenticatedUser().get();
		boolean wasDeleted = false;
		if (owner.getId().equals(hotel.getOwnerId()) || owner.getAuthorities().contains(UserRole.ADMIN)) {
			wasDeleted = hotelRepository.delete(hotelId);
		}
		if (!wasDeleted) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only owner and admin can delete hotels");
		}
	}

	public List<Hotel> getAll() {
		return hotelRepository.getHotels();
	}


	public Hotel update(Hotel hotel) {
		// 4. todo: only the owner of the hotel or admin may update the hotel
		var searchedHotel = hotelRepository.getById(hotel.getId()).get();
		var owner = getAuthenticatedUser().get();
		if (owner.getId().equals(searchedHotel.getOwnerId()) || owner.getAuthorities().contains(UserRole.ADMIN)) {
			getById(hotel.getId());
			return hotelRepository.save(hotel);
		}
		throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Only owner and admin can update hotels");
	}

	public Hotel create(Hotel hotel) {
		return hotelRepository.save(hotel);
	}

	public Hotel getById(UUID hotelId) {
		return hotelRepository.getById(hotelId).orElseThrow();
	}

	private Optional<User> getAuthenticatedUser(){
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return userRepository.findByUsername(authentication.getName());
	}
}
