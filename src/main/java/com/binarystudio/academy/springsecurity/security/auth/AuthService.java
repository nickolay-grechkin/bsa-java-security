package com.binarystudio.academy.springsecurity.security.auth;

import com.binarystudio.academy.springsecurity.domain.user.UserService;
import com.binarystudio.academy.springsecurity.domain.user.model.User;
import com.binarystudio.academy.springsecurity.security.auth.model.*;
import com.binarystudio.academy.springsecurity.security.jwt.JwtProvider;
import com.nimbusds.jose.crypto.PasswordBasedDecrypter;
import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;

@Service
public class AuthService {
    private final UserService userService;
    private final JwtProvider jwtProvider;
    private final PasswordEncoder passwordEncoder;

    public AuthService(UserService userService, JwtProvider jwtProvider, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthResponse performLogin(AuthorizationRequest authorizationRequest) {
        var userDetails = userService.loadUserByUsername(authorizationRequest.getUsername());
        if (passwordsDontMatch(authorizationRequest.getPassword(), userDetails.getPassword())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid password");
        }
        // 2. todo: auth and refresh token are given to user
        return AuthResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    public AuthResponse performRegistration(RegistrationRequest registrationRequest) {
        User user = new User();
        user.setUsername(registrationRequest.getLogin());
        user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
        user.setEmail(registrationRequest.getEmail());
        User userDetails = userService.registerUser(user);
        return AuthResponse.of(jwtProvider.generateToken(userDetails), jwtProvider.generateRefreshToken(userDetails));
    }

    private boolean passwordsDontMatch(String rawPw, String encodedPw) {
        return !passwordEncoder.matches(rawPw, encodedPw);
    }

    public AuthResponse refreshUserTokens(RefreshTokenRequest refreshTokenRequest) {
        String username = jwtProvider.getLoginFromToken(refreshTokenRequest.getRefreshToken());
        User user = userService.loadUserByUsername(username);
        return AuthResponse.of(jwtProvider.generateToken(user), jwtProvider.generateRefreshToken(user));
    }

    public AuthResponse changePassword(PasswordChangeRequest passwordChangeRequest) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user;
        if (authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Not authorized user can't change password ");
        } else {
            user = userService.loadUserByUsername(authentication.getName());
            user.setPassword(passwordEncoder.encode(passwordChangeRequest.getNewPassword()));
        }
        return AuthResponse.of(jwtProvider.generateToken(user), jwtProvider.generateRefreshToken(user));
    }

    public void generateTokenByEmail(String email) {
        User user = userService.getUserByEmail(email);
        System.out.println(jwtProvider.generateToken(user));
    }

    public AuthResponse changeForgottenPassword(ForgottenPasswordReplacementRequest forgottenPasswordReplacementRequest) {
        String username = jwtProvider.getLoginFromToken(forgottenPasswordReplacementRequest.getToken());
        User user = userService.loadUserByUsername(username);
        if (user != null) {
            user.setPassword(passwordEncoder.encode(forgottenPasswordReplacementRequest.getNewPassword()));
        }
        return AuthResponse.of(jwtProvider.generateToken(Objects.requireNonNull(user)), jwtProvider.generateRefreshToken(user));
    }
}
